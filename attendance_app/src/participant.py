from duration import *


class Participant:
    
    def __init__(self, full_name: str, duration: Duration):
        self.full_name = full_name
        self.duration = duration