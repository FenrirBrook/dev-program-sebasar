class Duration:
    """
    The Duration class represents a time value like 1h 32m 38s
    """

    def __init__(self, hours, minutes, seconds):
        self.__hours = hours
        self.__minutes = minutes
        self.__seconds = seconds

    @property
    def hours(self):
        return self.__hours

    @property
    def minutes(self):
        return self.__minutes

    @property
    def seconds(self):
        return self.__seconds

    def __str__(self):
        """ 
        This method overwrites default implementation to return `Duration` object as str in this format 1h 32m 38s
        """
        pass


class Participant:
    """
    The class Participant repressents an attendee in a meeting ocurrence
    """
    
    def __init__(self, name, email, first_join, last_leave, role):
        """Create a new Participant object

        :param name: The name of the participant, str object
        :param email: The email of the participant, str object
        :param first_join: DateTimeWrapper object
        :param last_leave: DateTimeWrapper object
        param role: str object
        """

        self.__name = name
        self.__email = email 
        self.__first_join = first_join
        self.__last_leave = last_leave 
        self.__role = role

    @property
    def in_meeting_duration(self):
        """
        This is the property that gets the in-meeting duration
        :return: `Duration` object, the elapsed time between :attr:`__first_join` and :attr:`__last_leave`
        """
        pass

    @property
    def name(self):
        pass

    @property
    def email(self):
        pass

    @property
    def first_join(self):
        pass

    @property
    def last_leave(self):
        pass

    @property
    def role(self):
        pass


class Meeting:
    """
    The Meeting represents a Team's meeting
    """

    def __init__(self, title):
        """
        Create a new Meetig object.

        :param title: The title of the meeting
        """
        self.__title = title
        self.__attendance = []

    @property
    def title(self):
        pass

    @property
    def attendance(self):
        """ This property gets the list of meeting ocurrences
        :return: The value of :attr:`__attendance`
        """
        pass

    def add_attendance(self, meeting_ocurrences):
        """ 
        This method adds 1 or more meeting ocurrences in the :attr:`__attendance`
        if any `MeetingOcurrence.start_time` and `MeetingOcurrence.end_time` exists already in the :attr:`__attendance` 
        then update all MeetingOcurrence attributes except `MeetingOcurrence.start_time` and `MeetingOcurrence.end_time`
        :param meeting_ocurrences: A list containing 1 or more `MeetingOcurrence` objects
        """
        pass


class MeetingOcurrence:
    """
    The MeetingOcurrence class represents a Team's meeting ocurrence
    """

    def __init__(self, attended, start_time, end_time):
        """
        Create a new MeetingOcurrence.

        :param attended: The number of participants
        :param start_time: The meeting start time, DateTimeWrapper object
        :paran end_time: The meeting end time, DateTimeWrapper object
        """
        self.__attended = attended
        self.__start_time = start_time
        self.__end_time = end_time
        self.__participants = []

    @property
    def meeting_duration(self):
        """
        This is the property that gets the meeting duration
        :return: The meeting duration, elapsed time between :attr:`__start_time` and :attr:`__end_time` as `Duration` object
        """
        pass
     
    @property
    def participants(self):
        """ This property returns the list of participants
        :return: The value of :attr:`__participants`
        """
        pass

    def add_participants(self, participants):
        """ 
        This method adds 1 or more participants in the :attr:`__participants`
        if `Participant.email` exists already update all Participant attributes except `Participant.email`
        :param participants: A list containing 1 or more `Participant` objects
        """
        pass
